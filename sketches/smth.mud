fun f (x, y = 33) z =
    x + y * z

fun g () =
    f 3 4 + f (5, y = 6) 7


signature Set (element_type : Eq) =
    type A : Iterable element_type, Sizeable, Container element_type, Eq

    val empty : A

    method add : A -> element_type -> A
    method remove : A -> element_type -> A

    method union : A -> A -> A
    method intersection : A -> A -> A

    method addAll : A -> Iterable element_type -> A * int
    method removeAll : A -> Iterable element_type -> A * int

    method contains : A -> element_type -> bool

    prop empty_is_empty : forall e : element_type,
                              contains empty e = false
    prop add_adds : forall (s : A, e : element_type),
                        contains (add A e) e = true
    prop remove_removes : forall (s : A, e : element_type),
                              contains (remove A e) e = false
    prop remove_removes_one : forall (s : A, e : element_type, e' : element_type),
                                  e <> e' => contains A e = contains (remove A e') e
    prop add_is_union_r : forall (s : A, e : element_type),
                              equal (add A e) (union A (add empty e)) = true

end

module TreeSet (element_type : Eq) :> Set element_type =
    datatype Tree = Node of {left : Tree, value : element_type, right : Tree}
                  | Leaf
    type A = Tree
    val empty = Leaf

    method add T e = Node {left = T, value = e, right = Leaf}
    ...
end

module ListSetInt :> Set Int =
    type A = List Int

    val empty = []

    method contains [] e = false
         | contains (e' :: es) e =
             if e = e'
             then true
             else contains es e

    method add S e =
        if S.contains e
        then S
        else e :: S

    method remove [] e = []
         | remove (e' :: es) e =
             if e' = e
             then es
             else e' :: (remove es e)
    ...
end


S = TreeSet int

val x = S.empty
val y = S.add x (g ())
val z = y.add 321
