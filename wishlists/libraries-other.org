* OEIS
  - Big module with every sequence on https://www.oeis.org
  - Or maybe just a subset, initially.
* Compiler as a library
  - Give access to MudML AST data type.
  - Seems to fit the idea of starting compilation via interpreting a
    script using the compiler library.
